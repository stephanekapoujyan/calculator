# Calculator

![alt text](https://raw.githubusercontent.com/remialvado/calculator/master/app/Resources/images/calculette-step12.png "Calculette step12")

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Etape 1

Générer une application Angular et l'envoyer sur un repository Git.

## Etape 2

Intégrer la page.

Utilisation de SCSS.

Le résultat ne doit s'afficher que si l'on a lancé un calcul.

Liste des opérateurs : (+) Addition, (-) Soustraction, (*) Multiplication

## Etape 3

Faire le calcul et afficher le résultat de l'opération

## Etape 4

Extraire le processus de calcul dans une class Typescript `Calculatrice` qui contient :
- 3 attributs représentant les opérandes et l'opérateur
- un constructeur permettant d'initialiser de manière optionnel ces attributs
- des getters et des setters pour ces 3 attributs
- une méthode `calcul` qui va lancer le calcul en fonction des attributs

## Etape 5

Ecrire un service Angular `CalculatriceService` qui implémente la classe Calculatrice. Il servira à faire une passerelle entre Angular et la classe qui a été écrite à l'étape 4.

Il contiendra une methode `calcul` qui sera utilisé dans les composants.

## Etape 6

*Gestion des erreurs*

La méthode `Calculatrice.calcul` ne peut fonctionner que si tous les attributs de la classe sont non-null
Lancer des exceptions de type `Error` dans la méthode `Calculatrice.calcul` si un des attributs est null

## Etape 7

Implémenter la division, avec sa gestion des erreurs (notamment la division par 0)

## Etape 8

Ecrire les tests permettant de tester unitairement la méthode `calcul`

## Etape 9

Refactorer la class `Calculatrice` pour avoir une class par opérateur en TDD.
Chaque class doit implémenter une interface unique nommé `OperatorInterface`.

L'objectif de cette étape est d'avoir une classe dédiée à une opération.

## Etape 10

Refactorer le code en enlevant toutes les conditions, i.e., enlever tous les `if`

## Etape 11

Mettre en place la [lib NgXs](https://www.ngxs.io/) dans l'application.

Créer un `CalculState` qui permet de stocker les calculs qui ont été effectués, implémentant les actions `AddCalcul` et `GetCalculs`.

Bonus : Afficher la liste des calculs présent dans le store et ajouter le [plugin NgXs NgxsStoragePluginModule](https://www.ngxs.io/plugins/storage) permettant de faire persister les données du store dans le `localStorage`
